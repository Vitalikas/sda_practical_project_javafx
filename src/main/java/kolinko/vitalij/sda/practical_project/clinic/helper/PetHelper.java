package kolinko.vitalij.sda.practical_project.clinic.helper;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import kolinko.vitalij.sda.practical_project.clinic.util.LoggerUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.logging.Level;

public class PetHelper {
    public Stage getSuccessMsg(String message) {
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initStyle(StageStyle.UNDECORATED);
        dialogStage.setResizable(false);
        dialogStage.setTitle("Success");

        Text text = new Text(message);
        text.getStyleClass().add("text");

        Background background = new Background(new BackgroundFill(Color.rgb(91, 194, 156), CornerRadii.EMPTY, Insets.EMPTY));

        VBox vBox = new VBox(text);
        vBox.setBackground(background);
        vBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vBox);
        scene.getStylesheets().add(PetHelper.class.getResource("/css/styles.css").toExternalForm());
        dialogStage.setScene(scene);
        return dialogStage;
    }

    public Stage getErrorMsg(String message) {
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initStyle(StageStyle.UNDECORATED);
        dialogStage.setResizable(false);
        dialogStage.setTitle("Error");

        Text text = new Text(message);
        text.getStyleClass().add("text");

        Background background = new Background(new BackgroundFill(Color.rgb(255, 0, 0), CornerRadii.EMPTY, Insets.EMPTY));

        VBox vBox = new VBox(text);
        vBox.setBackground(background);
        vBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vBox);
        scene.getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
        dialogStage.setScene(scene);
        return dialogStage;
    }

    public void checkDatePicker(DatePicker dp) {
        dp.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (isIncorrectDob(dp)) {
                if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.ERROR, "Invalid", "Please check date of birth!", false))) {
                    dp.requestFocus();
                    dp.show();
                }
            }
        });
    }

    public boolean isIncorrectDob(DatePicker dp) {
        return dp.getValue() != null && dp.getValue().isAfter(LocalDate.now());
    }

    public byte[] imageToByteArray(Image image) {
        BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            ImageIO.write(bufferedImage, "png", stream);
        } catch (IOException e) {
            LoggerUtil.createLog(Level.SEVERE, "Failed to write image to OutputStream", e);
        }
        return stream.toByteArray();
    }

    public Image byteArrayToImage(byte[] imageData)  {
        ByteArrayInputStream stream = new ByteArrayInputStream(imageData);
        return new Image(stream);
    }

    public void addTip(Node node, String text) {
        Tooltip tooltip = new Tooltip(text);
        Tooltip.install(node, tooltip);
    }

    public ImageView makeImageView(String path) {
        Image image = new Image(getClass().getResourceAsStream(path));
        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(18);
        imageView.setFitHeight(18);
        return imageView;
    }
}
