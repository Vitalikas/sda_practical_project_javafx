package kolinko.vitalij.sda.practical_project.clinic.controller;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import kolinko.vitalij.sda.practical_project.clinic.dao.PetDao;
import kolinko.vitalij.sda.practical_project.clinic.dao.RaceDao;
import kolinko.vitalij.sda.practical_project.clinic.entity.Pet;
import kolinko.vitalij.sda.practical_project.clinic.entity.Race;
import kolinko.vitalij.sda.practical_project.clinic.helper.*;
import kolinko.vitalij.sda.practical_project.clinic.util.LoggerUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class PetController implements Initializable {
    @FXML public ComboBox<String> comboBoxSearchBy;
    @FXML public TextField txtFieldSearch;
    @FXML private TableView<Pet> tableViewPets;
    @FXML private TableColumn<Pet, Integer> columnId;
    @FXML private TableColumn<Pet, String> columnRace;
    @FXML private TableColumn<Pet, LocalDate> columnDob;
    @FXML private TableColumn<Pet, Boolean> columnIsVaccinated;
    @FXML private TableColumn<Pet, String> columnOwnerName;
    @FXML private TableColumn<Pet, Byte> columnImage;
    @FXML public ImageView imageViewPet;
    @FXML public ComboBox<String> comboBoxRace;
    @FXML public DatePicker datePickerDob;
    @FXML public CheckBox checkBoxIsVaccinated;
    @FXML public TextField txtFieldOwnerName;
    @FXML public Button btnAdd;
    @FXML public Button btnUpdate;
    @FXML public Button btnDelete;
    @FXML public Button btnRaceDb;
    @FXML public Button btnClose;
    @FXML public Button btnConsults;
    @FXML public Button btnPets;
    @FXML public Button btnVets;
    @FXML public Button btnBack;

    int selectedRowIndex;
    boolean isSelectedRowIsLast;

    // creating listener for combo box
    ChangeListener<Object> cbListener = (observable, oldValue, newValue) -> {
        if (newValue != null) {
            comboBoxRace.setStyle("-fx-background-color: #ffffff");
        }
        if (comboBoxRace.getEditor().getText().isEmpty()) {
            comboBoxRace.setStyle("-fx-border-color: red; -fx-focus-color: red;");
        }
    };
    // creating listener for date picker
    ChangeListener<LocalDate> dpListener = (observable, oldValue, newValue) -> {
        if (newValue != null) {
            datePickerDob.setStyle("-fx-background-color: #ffffff");
        }
        if (datePickerDob.getValue() == null) {
            datePickerDob.setStyle("-fx-border-color: red; -fx-focus-color: red;");
        }
    };
    // creating listener for text field
    ChangeListener<String> tfListener = (observable, oldValue, newValue) -> {
        if (newValue != null) {
            txtFieldOwnerName.setStyle("-fx-background-color: #ffffff");
        }
        if (txtFieldOwnerName.getText().isEmpty()) {
            txtFieldOwnerName.setStyle("-fx-text-box-border: red; -fx-focus-color: red;");
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnRace.setCellValueFactory(new PropertyValueFactory<>("race"));
        columnDob.setCellValueFactory(new PropertyValueFactory<>("birthdate"));
        columnIsVaccinated.setCellValueFactory(new PropertyValueFactory<>("isVaccinated"));
        // setting cell to checkbox
        columnIsVaccinated.setCellValueFactory(v -> new SimpleBooleanProperty(v.getValue().getIsVaccinated()));
        columnIsVaccinated.setCellFactory(tc -> new CheckBoxTableCell<>());

        columnOwnerName.setCellValueFactory(new PropertyValueFactory<>("ownerName"));
        columnImage.setCellValueFactory(new PropertyValueFactory<>("image"));
//        tablePets.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        btnClose.setGraphic(new PetHelper().makeImageView("/images/close.png"));
        btnRaceDb.setGraphic(new PetHelper().makeImageView("/images/edit.png"));
        btnAdd.setGraphic(new PetHelper().makeImageView("/images/add_green.png"));
        btnUpdate.setGraphic(new PetHelper().makeImageView("/images/save_green.png"));
        btnDelete.setGraphic(new PetHelper().makeImageView("/images/delete_red.png"));
        btnConsults.setGraphic(new PetHelper().makeImageView("/images/event.png"));
        btnPets.setGraphic(new PetHelper().makeImageView("/images/pet.png"));
        btnVets.setGraphic(new PetHelper().makeImageView("/images/vet.png"));
        btnBack.setGraphic(new PetHelper().makeImageView("/images/back.png"));

        Disabable.disableNodes(Arrays.asList(btnUpdate, btnDelete), true);

        fillTable();
        readTableRowData();
        fillComboBoxSearchBy();
        filterTable();
        addPet();
        updatePet();
        deletePet();
        new PetHelper().checkDatePicker(datePickerDob);
        setImageView();
        new PetHelper().addTip(imageViewPet, "Click to edit/upload image");
        filterComboBox();
        manageRaces();
        showVetWindow();
        showConsultsWindow();
        closeWindow();
        exitApp();
    }

    // reading data from database to table
    public void fillTable() {
        // getting list of pets from database
        List<Pet> pets = new PetDao().readPets();
        // converting list to observable
        ObservableList<Pet> oList = OList.makeObservableList(pets);
        // filling table with observable list
        OList.fillNode(tableViewPets, oList);
    }

    // reading table row data
    private void readTableRowData() {
        // adding event listener for selected rows in table
        tableViewPets.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (tableViewPets.getSelectionModel().getSelectedItem() != null) {
                // getting selected row index
                selectedRowIndex = tableViewPets.getSelectionModel().getSelectedIndex();
                // checking if record is the last
                isSelectedRowIsLast = Selectable.isLastRecordOfNode(tableViewPets);
                // focusing to selected race
                Focusable.focusToComboBoxRandomRecord(comboBoxRace, tableViewPets.getSelectionModel().getSelectedItem().getRace());
                // reading table row data
                String race = tableViewPets.getSelectionModel().getSelectedItem().getRace();
                comboBoxRace.getEditor().setText(race);
                LocalDate dob = tableViewPets.getSelectionModel().getSelectedItem().getBirthdate();
                datePickerDob.setValue(dob);
                Boolean isVaccinated = tableViewPets.getSelectionModel().getSelectedItem().getIsVaccinated();
                checkBoxIsVaccinated.setSelected(isVaccinated);
                String ownerName = tableViewPets.getSelectionModel().getSelectedItem().getOwnerName();
                txtFieldOwnerName.setText(ownerName);
                Image image = new PetHelper().byteArrayToImage(tableViewPets.getSelectionModel().getSelectedItem().getImage());
                imageViewPet.setImage(image);
                // enabling nodes
                Disabable.disableNodes(Arrays.asList(
                        btnUpdate,
                        btnDelete,
                        comboBoxRace,
                        datePickerDob,
                        checkBoxIsVaccinated,
                        txtFieldOwnerName,
                        imageViewPet), false);
            }
        });
    }

    // filling combo box with searching criteria
    public void fillComboBoxSearchBy() {
        OList.fillNode(comboBoxSearchBy, OList.makeObservableList(Arrays.asList("race", "owner", "id")));
        Focusable.focusToFirstRecordOfNode(comboBoxSearchBy);
    }

    // filtering table
    public void filterTable() {
        FilteredList<Pet> flPet = new FilteredList<>(OList.makeObservableList(new PetDao().readPets()), pet -> true);
        OList.fillNode(tableViewPets, flPet);

        txtFieldSearch.setOnKeyReleased(event -> {
            switch (comboBoxSearchBy.getValue()) {
                case "race":
                    flPet.setPredicate(pet -> pet.getRace().toLowerCase().contains(txtFieldSearch.getText().toLowerCase().trim()));
                    break;
                case "owner":
                    flPet.setPredicate(pet -> pet.getOwnerName().toLowerCase().contains(txtFieldSearch.getText().toLowerCase().trim()));
                    break;
                case "id":
                    flPet.setPredicate(pet -> pet.getId().toString().contains(txtFieldSearch.getText().toLowerCase().trim()));
                    break;
            }

            // removing focus from selected record of table
            tableViewPets.getSelectionModel().clearSelection();
            // clearing fields
            Clearable.clearNodes(Arrays.asList(comboBoxRace, datePickerDob, checkBoxIsVaccinated, txtFieldOwnerName, imageViewPet), "/images/addimage.png");
            // disabling buttons
            Disabable.disableNodes(Arrays.asList(btnUpdate, btnDelete), true);
        });
    }

    // adding new record in database
    public void addPet() {
        btnAdd.setOnAction(event -> {
            if (Empty.isNotEmptyAllNodes(Arrays.asList(comboBoxRace, datePickerDob, txtFieldOwnerName))) {
                new PetDao().addPet(createPetPOJO());
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record added successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table filtering
                filterTable();
                // focusing to created race
                Focusable.focusToLastRecordOfNode(tableViewPets);
                // remove listeners
                comboBoxRace.valueProperty().removeListener(cbListener);
                datePickerDob.valueProperty().removeListener(dpListener);
                txtFieldOwnerName.textProperty().removeListener(tfListener);
            } else {
                if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.ERROR, "Input is required",
                        "Please fill out this field to continue", false))) {
                    Empty.focusToEmptyNodes(Arrays.asList(comboBoxRace, datePickerDob, txtFieldOwnerName));
                    // adding listeners
                    comboBoxRace.valueProperty().addListener(cbListener);
                    datePickerDob.valueProperty().addListener(dpListener);
                    txtFieldOwnerName.textProperty().addListener(tfListener);
                }
            }
        });
    }

    // creating POJO
    private Pet createPetPOJO() {
        return new Pet(
                comboBoxRace.getValue(),
                datePickerDob.getValue(),
                checkBoxIsVaccinated.isSelected(),
                txtFieldOwnerName.getText(),
                new PetHelper().imageToByteArray(imageViewPet.getImage()));
    }

    // updating record
    private void updatePet() {
        btnUpdate.setOnAction(event -> {
            if (Empty.isNotEmptyAllNodes(Arrays.asList(comboBoxRace, datePickerDob, txtFieldOwnerName))) {
                new PetDao().updatePet(tableViewPets, comboBoxRace, datePickerDob, checkBoxIsVaccinated, txtFieldOwnerName, imageViewPet);
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record updated successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table
                filterTable();
                // focusing to updated row
                Focusable.focusToSelectedRow(tableViewPets, selectedRowIndex);
                // remove listeners
                comboBoxRace.valueProperty().removeListener(cbListener);
                datePickerDob.valueProperty().removeListener(dpListener);
                txtFieldOwnerName.textProperty().removeListener(tfListener);
            } else {
                if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.ERROR, "Input is required",
                        "Please fill out this field to continue", false))) {
                    Empty.focusToEmptyNodes(Arrays.asList(comboBoxRace, datePickerDob, txtFieldOwnerName));
                    // adding listeners
                    comboBoxRace.valueProperty().addListener(cbListener);
                    datePickerDob.valueProperty().addListener(dpListener);
                    txtFieldOwnerName.textProperty().addListener(tfListener);
                }
            }
        });
    }

    // deleting record
    private void deletePet() {
        btnDelete.setOnAction(event -> {
            if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.CONFIRMATION, "Confirmation",
                    "Are you sure you want to delete record from database?", false))) {
                new PetDao().deletePet(tableViewPets);
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record deleted successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table
                filterTable();
                // focusing to record
                if (isSelectedRowIsLast) {
                    Focusable.focusToPrevRecordOfNode(tableViewPets, selectedRowIndex);
                } else {
                    Focusable.focusToSelectedRow(tableViewPets, selectedRowIndex);
                }
                // clearing search field
                Clearable.clearNodes(Collections.singletonList(txtFieldSearch), null);
                // clearing nodes on empty table
                Clearable.clearNodesOnEmptyNode(Empty.isEmptyTable(Collections.singletonList(tableViewPets)),
                        Arrays.asList(
                                comboBoxRace,
                                datePickerDob,
                                checkBoxIsVaccinated,
                                txtFieldOwnerName,
                                txtFieldSearch,
                                imageViewPet), "/images/addimage.png", isEmpty -> isEmpty);
                // disabling nodes on empty table
                Disabable.disableNodesOnEmptyNode(Empty.isEmptyTable(Collections.singletonList(tableViewPets)),
                        Arrays.asList(btnUpdate, btnDelete), isEmpty -> isEmpty);
            }
        });
    }

    // creating file chooser for image uploading
    public File selectFileToUpload() {
        FileChooser fileChooser = new FileChooser();
        // adding file filters
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG file (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPEG file (*.jpeg)", "*.jpeg"),
                new FileChooser.ExtensionFilter("JPG file (*.jpg)", "*.jpg"));
        return fileChooser.showOpenDialog(imageViewPet.getScene().getWindow());
    }

    // setting image to image view
    public void setImageView() {
        imageViewPet.setOnMouseClicked(event -> {
            try {
                File selectedFile = selectFileToUpload();
                if (selectedFile == null) {
                    System.out.println(" ");
                } else {
                    Image image = new Image(new FileInputStream(selectedFile));
                    imageViewPet.setImage(image);
                }
            } catch (FileNotFoundException e) {
                LoggerUtil.createLog(Level.SEVERE, "File not found", e);
            }
        });
    }

    // filtering combo box
    public void filterComboBox() {
        FilteredList<Race> fl = new FilteredList<>(OList.makeObservableList(new RaceDao().readRaces()), race -> true);
        OList.fillNode(comboBoxRace, fl);

        comboBoxRace.setOnKeyReleased(event -> {
            fl.setPredicate(pet -> pet.getRace().toLowerCase().contains(comboBoxRace.getEditor().getText().toLowerCase().trim()));
            comboBoxRace.show();
        });
    }

    // showing new window
    public void showStageAndWait(String pathToFxml) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(pathToFxml));
        try {
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to show stage", e);
        }
    }

    // managing races
    private void manageRaces() {
        btnRaceDb.setOnAction(event -> {
            showStageAndWait("/fxml/race.fxml");
            // adding listener for table filtering
            filterTable();
            // focusing to updated row
            Focusable.focusToSelectedRow(tableViewPets, selectedRowIndex);
            // filtering combo box
            filterComboBox();
        });
    }

    // managing vets
    private void showVetWindow() {
        btnVets.setOnAction(event -> showStageAndWait("/fxml/vet.fxml"));
    }

    // managing consults
    private void showConsultsWindow() {
        btnConsults.setOnAction(event -> showStageAndWait("/fxml/consult.fxml"));
    }

    // close window
    public void closeWindow() {
        Closeable.closeWindowOnButtonClick(btnBack);
    }

    // exit app
    public void exitApp() {
        Closeable.exitApp(btnClose);
    }
}
