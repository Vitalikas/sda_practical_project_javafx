package kolinko.vitalij.sda.practical_project.clinic.helper;

import javafx.scene.Node;
import javafx.scene.control.TableView;

public class Selectable {
    public static boolean isLastRecordOfNode(Node node) {
        boolean isLast = false;
        if (node instanceof TableView) {
            if (((TableView) node).getSelectionModel().getSelectedIndex() == ((TableView) node).getItems().size()-1) {
                isLast = true;
            }
        }
        return isLast;
    }
}
