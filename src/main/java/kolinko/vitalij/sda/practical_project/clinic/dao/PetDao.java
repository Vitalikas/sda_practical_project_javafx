package kolinko.vitalij.sda.practical_project.clinic.dao;

import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import kolinko.vitalij.sda.practical_project.clinic.entity.Pet;
import kolinko.vitalij.sda.practical_project.clinic.helper.PetHelper;
import kolinko.vitalij.sda.practical_project.clinic.util.HibernateUtil;
import kolinko.vitalij.sda.practical_project.clinic.util.LoggerUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class PetDao {
    // reading records from Pet database
    public List<Pet> readPets() {
        List<Pet> pets = new ArrayList<>();
        Transaction transaction = null;
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            pets = session.createQuery("FROM Pet", Pet.class).list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to get records from database", e);
        }
        return pets;
    }

    public void addPet(Pet pet) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            // creating new record in database
            session.save(pet);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to add new record to database", e);
        }
    }

    // updating record in database
    public void updatePet(TableView<Pet> tv, ComboBox<String> cb, DatePicker dp, CheckBox chb, TextField tf, ImageView imageView) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Pet petToUpdate = session.load(Pet.class, tv.getSelectionModel().getSelectedItem().getId());
            // setting row properties
            petToUpdate.setRace(cb.getValue());
            petToUpdate.setBirthdate(dp.getValue());
            petToUpdate.setIsVaccinated(chb.isSelected());
            petToUpdate.setOwnerName(tf.getText());
            // converting image to byte array
            byte[] imageData = new PetHelper().imageToByteArray(imageView.getImage());
            petToUpdate.setImage(imageData);
            // updating record in database
            session.update(petToUpdate);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to update record in database", e);
        }
    }

    // deleting record from database
    public void deletePet(TableView<Pet> tableView) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Pet petToDelete = session.load(Pet.class, tableView.getSelectionModel().getSelectedItem().getId());
            // deleting record from database
            session.delete(petToDelete);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to delete record from database", e);
        }
    }
}
