package kolinko.vitalij.sda.practical_project.clinic.controller;

import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import kolinko.vitalij.sda.practical_project.clinic.dao.RaceDao;
import kolinko.vitalij.sda.practical_project.clinic.entity.Race;
import kolinko.vitalij.sda.practical_project.clinic.helper.*;

import java.net.URL;
import java.util.*;

public class RaceController implements Initializable {
    @FXML public TableView<Race> tableViewRaces;
    @FXML private TableColumn<Race, Integer> columnId;
    @FXML private TableColumn<Race, String> columnRace;
    @FXML public TextField txtFieldRace;
    @FXML public TextField txtFieldSearch;
    @FXML public Button btnAddRace;
    @FXML public Button btnSaveRace;
    @FXML public Button btnDeleteRace;
    @FXML public Button btnClose;
    int selectedRowIndex;
    boolean isSelectedRowIsLast;

    // creating listener for text field
    ChangeListener<String> tfListener = (observable, oldValue, newValue) -> {
        if (newValue != null) {
            txtFieldRace.setStyle("-fx-background-color: #ffffff");
        }
        if (txtFieldRace.getText().isEmpty()) {
            txtFieldRace.setStyle("-fx-text-box-border: red; -fx-focus-color: red;");
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnRace.setCellValueFactory(new PropertyValueFactory<>("race"));
        fillTable();
        readTableRowData();
        addRace();
        updateRace();
        deleteRace();
        filterTable();
        close();
        Disabable.disableNodes(Arrays.asList(btnSaveRace, btnDeleteRace), true);
        btnClose.setGraphic(new PetHelper().makeImageView("/images/close.png"));
        btnAddRace.setGraphic(new PetHelper().makeImageView("/images/add_green.png"));
        btnSaveRace.setGraphic(new PetHelper().makeImageView("/images/save_green.png"));
        btnDeleteRace.setGraphic(new PetHelper().makeImageView("/images/delete_red.png"));
    }

    // reading data from database to table
    public void fillTable() {
        // getting list of races from database
        List<Race> races = new RaceDao().readRaces();
        // converting list to observable
        ObservableList<Race> oList = OList.makeObservableList(races);
        // filling table with observable list
        OList.fillNode(tableViewRaces, oList);
    }

    // filling text field with data from table
    private void readTableRowData() {
        // adding event listener for selected rows in table
        tableViewRaces.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (tableViewRaces.getSelectionModel().getSelectedItem() != null) {
                // getting selected row index
                selectedRowIndex = tableViewRaces.getSelectionModel().getSelectedIndex();
                // checking if record is the last
                isSelectedRowIsLast = Selectable.isLastRecordOfNode(tableViewRaces);

                String race = tableViewRaces.getSelectionModel().getSelectedItem().getRace();
                txtFieldRace.setText(race);
                // enabling nodes
                Disabable.disableNodes(Arrays.asList(btnSaveRace, btnDeleteRace), false);
            }
        });
    }

    // adding new race to database
    public void addRace() {
        btnAddRace.setOnAction(event -> {
            if (Empty.isNotEmptyAllNodes(Collections.singletonList(txtFieldRace))) {
                if (new RaceDao().isDuplicate(createRacePOJO())) {
                    // getting stage
                    Stage msgWindow = new PetHelper().getErrorMsg("Record already exists");
                    // showing success message
                    msgWindow.show();
                    // auto closing message window on custom timer
                    Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                    txtFieldRace.requestFocus();
                }
                else {
                    new RaceDao().createRace(createRacePOJO());
                    // getting stage
                    Stage msgWindow = new PetHelper().getSuccessMsg("Record added successfully");
                    // showing success message
                    msgWindow.show();
                    // auto closing message window on custom timer
                    Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                    // refreshing table
                    fillTable();
                    // adding listener for table filtering
                    filterTable();
                    // focusing to created race
                    Focusable.focusToLastRecordOfNode(tableViewRaces);
                    // enabling nodes
                    Disabable.disableNodesOnEmptyNode(Empty.isEmptyTable(Collections.singletonList(tableViewRaces)),
                            Arrays.asList(btnSaveRace, btnDeleteRace), notEmpty -> notEmpty);
                    // remove listener
                    txtFieldRace.textProperty().removeListener(tfListener);
                }
            } else {
                if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.ERROR, "Input is required",
                        "Please fill out this field to continue", false))) {
                    Empty.focusToEmptyNodes(Collections.singletonList(txtFieldRace));
                    // adding listener
                    txtFieldRace.textProperty().addListener(tfListener);
                }
            }
        });
    }

    // creating race POJO
    private Race createRacePOJO() {
        return new Race(txtFieldRace.getText());
    }

    // updating selected race
    public void updateRace() {
        btnSaveRace.setOnAction(event -> {
            if (Empty.isNotEmptyAllNodes(Collections.singletonList(txtFieldRace))) {
                new RaceDao().updateRace(tableViewRaces, txtFieldRace);
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record updated successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table filtering
                filterTable();
                // focusing to updated row
                Focusable.focusToSelectedRow(tableViewRaces, selectedRowIndex);
            } else {
                if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.ERROR, "Input is required",
                        "Please fill out this field to continue", false))) {
                    Empty.focusToEmptyNodes(Collections.singletonList(txtFieldRace));
                }
            }
        });
    }

    // deleting race from database
    public void deleteRace() {
        btnDeleteRace.setOnAction(event -> {
            if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.CONFIRMATION, "Confirmation",
                    "Are you sure you want to delete record from database?", false))) {
                new RaceDao().deleteRace(tableViewRaces);
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record deleted successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table filtering
                filterTable();
                // focusing to record
                if (isSelectedRowIsLast) {
                    Focusable.focusToPrevRecordOfNode(tableViewRaces, selectedRowIndex);
                } else {
                    Focusable.focusToSelectedRow(tableViewRaces, selectedRowIndex);
                }
                // clearing search field
                Clearable.clearNodes(Collections.singletonList(txtFieldSearch), null);
                // clearing nodes on empty table
                Clearable.clearNodesOnEmptyNode(Empty.isEmptyTable(Collections.singletonList(tableViewRaces)),
                        Collections.singletonList(txtFieldRace), null, isEmpty -> isEmpty);
                // disabling nodes on empty table
                Disabable.disableNodesOnEmptyNode(Empty.isEmptyTable(Collections.singletonList(tableViewRaces)),
                        Arrays.asList(btnSaveRace, btnDeleteRace), isEmpty -> isEmpty);
            }
        });
    }

    // searching race
    public void filterTable() {
        FilteredList<Race> flRace = new FilteredList<>(OList.makeObservableList(new RaceDao().readRaces()), race -> true);
        OList.fillNode(tableViewRaces, flRace);

        txtFieldSearch.setOnKeyReleased(event -> {
            flRace.setPredicate(pet -> pet.getRace().toLowerCase().contains(txtFieldSearch.getText().toLowerCase().trim()));

            // removing focus from selected record of table
            tableViewRaces.getSelectionModel().clearSelection();
            // clearing fields
            Clearable.clearNodes(Collections.singletonList(txtFieldRace), null);
            // disabling buttons
            Disabable.disableNodes(Arrays.asList(btnSaveRace, btnDeleteRace), true);
        });
    }

    // closing window
    public void close() {
        Closeable.closeWindowOnButtonClick(btnClose);
    }
}
