package kolinko.vitalij.sda.practical_project.clinic.helper;

import javafx.scene.Node;

import java.util.List;
import java.util.function.Predicate;

public interface Disabable {
    static void disableNodes(List<Node> nodes, Boolean b) {
        nodes.forEach(node -> node.setDisable(b));
    }

    static void disableNodesOnEmptyNode(boolean isEmpty, List<Node> nodes, Predicate<Boolean> predicate) {
        if (predicate.test(isEmpty)) {
            nodes.forEach(node -> node.setDisable(true));
        } else {
            nodes.forEach(node -> node.setDisable(false));
        }
    }
}
