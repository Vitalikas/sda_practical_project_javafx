package kolinko.vitalij.sda.practical_project.clinic;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import kolinko.vitalij.sda.practical_project.clinic.entity.Consult;
import kolinko.vitalij.sda.practical_project.clinic.entity.Pet;
import kolinko.vitalij.sda.practical_project.clinic.entity.Race;
import kolinko.vitalij.sda.practical_project.clinic.entity.Vet;
import kolinko.vitalij.sda.practical_project.clinic.helper.PetHelper;
import kolinko.vitalij.sda.practical_project.clinic.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.time.LocalDate;
import java.util.Arrays;

public class Main extends Application {
    public static void main(String[] args) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            Consult consult1 = new Consult(LocalDate.of(2020, 9, 20),
                    "dantų valymas");
            Consult consult2 = new Consult(LocalDate.of(2020, 9, 22),
                    "čipavimas");
            Consult consult3 = new Consult(LocalDate.of(2020, 9, 25),
                    "kastravimas");

            Vet vet1 = new Vet(
                    "Tadas",
                    "Mažeika",
                    "Kauno g. 2",
                    "chirurgas",
                    new PetHelper().imageToByteArray(new Image(Main.class.getResourceAsStream("/images/vet1.png"))));
            Vet vet2 = new Vet(
                    "Andrius",
                    "Valinskas",
                    "Vilniaus g. 18",
                    "veterinaras",
                    new PetHelper().imageToByteArray(new Image(Main.class.getResourceAsStream("/images/vet2.png"))));

            Pet pet1 = new Pet(
                    "Cat",
                    LocalDate.of(2018, 5, 26),
                    true,
                    "Mindaugas Savickas",
                    new PetHelper().imageToByteArray(new Image(Main.class.getResourceAsStream("/images/chartreux.png"))));
            Pet pet2 = new Pet(
                    "Cat",
                    LocalDate.of(2015, 2, 10),
                    true,
                    "Robertas Antanovas",
                    new PetHelper().imageToByteArray(new Image(Main.class.getResourceAsStream("/images/singapura.png"))));
            Pet pet3 = new Pet("Dog",
                    LocalDate.of(2020, 9, 3),
                    false,
                    "Inga Kairienė",
                    new PetHelper().imageToByteArray(new Image(Main.class.getResourceAsStream("/images/addimage.png"))));

            consult1.setVet(vet2);
            consult1.setPet(pet1);
            consult2.setVet(vet1);
            consult2.setPet(pet2);
            consult3.setVet(vet1);
            consult3.setPet(pet3);

            Race race1 = new Race("Dog");
            Race race2 = new Race("Cat");
            Race race3 = new Race("Bird");

            saveAll(session, Arrays.asList(vet1, vet2));
            saveAll(session, Arrays.asList(pet1, pet2, pet3));
            saveAll(session, Arrays.asList(consult1, consult2, consult3));
            saveAll(session, Arrays.asList(race1, race2, race3));

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
                e.printStackTrace();
            }
            e.printStackTrace();
        }

        launch(args);

        // shutting down
        HibernateUtil.shutdown();
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader3 = new FXMLLoader(getClass().getResource("/fxml/consult.fxml"));
        Parent root3 = fxmlLoader3.load();
        Scene scene3 = new Scene(root3);
        stage.setScene(scene3);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setResizable(false);
        stage.show();
    }

    private static <T> void saveAll(Session session, Iterable<T> entities) {
        entities.forEach(session::saveOrUpdate);
    }
}
