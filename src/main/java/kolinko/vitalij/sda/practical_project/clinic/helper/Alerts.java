package kolinko.vitalij.sda.practical_project.clinic.helper;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public interface Alerts {
    static Alert showAlert(Alert.AlertType alertType, String title, String contentText, Boolean isResizable) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setContentText(contentText);
        alert.setResizable(isResizable);
        alert.showAndWait();
        return alert;
    }

    static boolean isConfirmed(Alert alert) {
        return alert.getResult() == ButtonType.OK;
    }
}
