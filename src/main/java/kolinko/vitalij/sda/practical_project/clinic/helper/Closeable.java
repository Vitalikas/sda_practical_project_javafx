package kolinko.vitalij.sda.practical_project.clinic.helper;

import javafx.animation.PauseTransition;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.List;

public interface Closeable {
    static void closeWindowOnButtonClick(Button button) {
        button.setOnAction(event -> {
            Stage stage = (Stage) button.getScene().getWindow();
            stage.close();
        });
    }

    static void exitApp(Button button) {
        button.setOnAction(event -> System.exit(0));
    }

    static void closeWindowsAfterDelay(List<Stage> stages, Double sec) {
        PauseTransition delay = new PauseTransition(Duration.seconds(sec));
        delay.setOnFinished(event -> stages.forEach(Stage::close));
        delay.play();
    }
}
