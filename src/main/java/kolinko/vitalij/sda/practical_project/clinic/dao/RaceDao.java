package kolinko.vitalij.sda.practical_project.clinic.dao;

import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import kolinko.vitalij.sda.practical_project.clinic.entity.Race;
import kolinko.vitalij.sda.practical_project.clinic.util.HibernateUtil;
import kolinko.vitalij.sda.practical_project.clinic.util.LoggerUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class RaceDao {
    // checking for duplicates in database
    public boolean isDuplicate(Race race) {
        boolean isDuplicate = false;
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            // checking for race duplicate in database
            Query query = session.createQuery("FROM Race r WHERE r.race=:race");
            query.setParameter("race", race.getRace());
            Race result = (Race) query.getResultStream().findFirst().orElse(null);
            isDuplicate = result != null;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to check for duplicates in database", e);
        }
        return isDuplicate;
    }

    // creating new record in database
    public void createRace(Race race) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            // creating new record in database
            session.save(race);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to add new record to database", e);
        }
    }

    // reading records from database
    public List<Race> readRaces() {
        List<Race> races = new ArrayList<>();
        Transaction transaction = null;
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            races = session.createQuery("FROM Race").list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to get records from database", e);
        }
        return races;
    }

//    public Race getSelectedRace(ComboBox<String> cb) {
//        Transaction transaction = null;
//        Race raceFromDatabase = null;
//        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
//            transaction = session.beginTransaction();
//            Query query = session.createQuery("FROM Race r WHERE r.race=:race");
//            query.setParameter("race", cb.getSelectionModel().getSelectedItem());
//            Race race = (Race) query.getResultStream().findFirst().orElse(null);
//            if (race != null) {
//                // loading race from database
//                raceFromDatabase = session.load(Race.class, race.getId());
//                transaction.commit();
//            }
//        } catch (Exception e) {
//            if (transaction != null) {
//                transaction.rollback();
//            }
//            // logging errors
//            LoggerUtil.createLog(Level.SEVERE, "", e);
//        }
//        return raceFromDatabase;
//    }

    // updating record in database
    public void updateRace(TableView<Race> tv, TextField tf) {
        Transaction transaction = null;
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Race raceToUpdate = session.load(Race.class, tv.getSelectionModel().getSelectedItem().getId());
            raceToUpdate.setRace(tf.getText());
            // updating race
            session.update(raceToUpdate);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to update record in database", e);
        }
    }

    // deleting record from database
    public void deleteRace(TableView<Race> tv) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Race raceToDelete = session.load(Race.class, tv.getSelectionModel().getSelectedItem().getId());
            // deleting record from database
            session.delete(raceToDelete);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to delete record from database", e);
        }
    }
}
