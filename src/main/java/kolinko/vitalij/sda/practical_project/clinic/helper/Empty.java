package kolinko.vitalij.sda.practical_project.clinic.helper;

import javafx.scene.Node;
import javafx.scene.control.*;

import java.util.List;
import java.util.stream.Collectors;

public interface Empty {
    static boolean isNotEmptyAllNodes(List<Node> nodes) {
        return nodes.stream()
                .filter(node -> node instanceof ComboBox)
                .map(node -> (ComboBox<String>) node)
                .collect(Collectors.toList())
                .stream()
                .noneMatch(comboBox -> comboBox.getEditor().getText().isEmpty()) &&
                nodes.stream()
                        .filter(node -> node instanceof DatePicker)
                        .map(node -> (DatePicker) node)
                        .collect(Collectors.toList())
                        .stream()
                        .allMatch(datePicker -> datePicker.getValue() != null) &&
                nodes.stream()
                        .filter(node -> node instanceof TextField)
                        .map(node -> (TextField) node)
                        .collect(Collectors.toList())
                        .stream()
                        .noneMatch(textField -> textField.getText().isEmpty());
    }

    static <T> void focusToEmptyNodes(List<Node> nodes) {
        nodes.stream()
                .filter(node -> node instanceof ComboBox)
                .filter(node -> ((ComboBox<T>) node).getSelectionModel().isEmpty())
                .findFirst()
                .ifPresent(node -> {
                    node.requestFocus();
                    node.setStyle("-fx-border-color: red; -fx-focus-color: red;");
                });
        nodes.stream()
                .filter(node -> node instanceof DatePicker)
                .filter(node -> ((DatePicker) node).getValue() == null)
                .findFirst()
                .ifPresent(node -> {
                    node.requestFocus();
                    node.setStyle("-fx-border-color: red; -fx-focus-color: red;");
                    ((DatePicker) node).show();
                });
        nodes.stream()
                .filter(node -> node instanceof TextField)
                .filter(node -> ((TextField) node).getText().isEmpty())
                .forEach(node -> {
                    node.requestFocus();
                    node.setStyle("-fx-text-box-border: red; -fx-focus-color: red;");
                });
    }

    static <T> boolean isEmptyTable(List<Node> nodes) {
        return nodes.stream()
                .filter(node -> node instanceof TableView)
                .map(node -> (TableView<T>) node)
                .collect(Collectors.toList())
                .stream()
                .allMatch(tableView -> tableView.getItems().size() == 0);
    }
}
