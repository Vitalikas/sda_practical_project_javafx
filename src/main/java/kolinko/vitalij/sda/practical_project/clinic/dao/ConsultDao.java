package kolinko.vitalij.sda.practical_project.clinic.dao;

import javafx.scene.control.*;
import kolinko.vitalij.sda.practical_project.clinic.entity.Consult;
import kolinko.vitalij.sda.practical_project.clinic.entity.Pet;
import kolinko.vitalij.sda.practical_project.clinic.entity.Vet;
import kolinko.vitalij.sda.practical_project.clinic.util.HibernateUtil;
import kolinko.vitalij.sda.practical_project.clinic.util.LoggerUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class ConsultDao {
    // reading records from database
    public List<Consult> readConsults() {
        List<Consult> consults = new ArrayList<>();
        Transaction transaction = null;
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            consults = session.createQuery("FROM Consult", Consult.class).list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to get records from database", e);
        }
        return consults;
    }

    // creating new record in database
    public void addConsult(Consult consult) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            // creating new record in database
            session.save(consult);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to add new record to database", e);
        }
    }

    // updating record in database
    public void updateConsult(TableView<Consult> tv, DatePicker dp, TextField tf, ComboBox<Vet> cbV, ComboBox<Pet> cbP) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Consult consultToUpdate = session.load(Consult.class, tv.getSelectionModel().getSelectedItem().getId());
            // setting row properties
            consultToUpdate.setDate(dp.getValue());
            consultToUpdate.setDescription(tf.getText());
            consultToUpdate.setVet(cbV.getValue());
            consultToUpdate.setPet(cbP.getValue());
            // updating record in database
            session.update(consultToUpdate);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to update record in database", e);
        }
    }

    // deleting record from database
    public void deleteConsult(TableView<Consult> tableView) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Consult consultToDelete = session.load(Consult.class, tableView.getSelectionModel().getSelectedItem().getId());
            // deleting record from database
            session.delete(consultToDelete);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to delete record from database", e);
        }
    }
}
