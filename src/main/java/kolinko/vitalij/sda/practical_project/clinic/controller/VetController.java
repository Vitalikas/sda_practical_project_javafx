package kolinko.vitalij.sda.practical_project.clinic.controller;

import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import kolinko.vitalij.sda.practical_project.clinic.dao.VetDao;
import kolinko.vitalij.sda.practical_project.clinic.entity.Vet;
import kolinko.vitalij.sda.practical_project.clinic.helper.*;
import kolinko.vitalij.sda.practical_project.clinic.util.LoggerUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class VetController implements Initializable {
    @FXML private ComboBox<String> comboBoxSearchBy;
    @FXML private TextField txtFieldSearch;
    @FXML private TableView<Vet> tableViewVets;
    @FXML private TableColumn<Vet, Integer> columnId;
    @FXML private TableColumn<Vet, String> columnFirstname;
    @FXML private TableColumn<Vet, String> columnLastname;
    @FXML private TableColumn<Vet, String> columnAddress;
    @FXML private TableColumn<Vet, String> columnSpeciality;
    @FXML private TableColumn<Vet, Byte> columnImage;
    @FXML public ImageView imageViewVet;
    @FXML public TextField txtFieldFirstname;
    @FXML public TextField txtFieldLastname;
    @FXML public TextField txtFieldAddress;
    @FXML public TextField txtFieldSpeciality;
    @FXML public Button btnAdd;
    @FXML public Button btnUpdate;
    @FXML public Button btnDelete;
    @FXML public Button btnClose;
    @FXML public Button btnConsults;
    @FXML public Button btnPets;
    @FXML public Button btnVets;
    @FXML public Button btnBack;

    int selectedRowIndex;
    boolean isSelectedRowIsLast;

    // creating listener for text fields
    ChangeListener<String> tfListener = (observable, oldValue, newValue) ->
        Arrays.asList(txtFieldFirstname, txtFieldLastname, txtFieldAddress, txtFieldSpeciality).forEach(textField -> {
            if (newValue != null) {
                textField.setStyle("-fx-background-color: #ffffff");
            }
            if (textField.getText().isEmpty()) {
                textField.setStyle("-fx-text-box-border: red; -fx-focus-color: red;");
            }
        });

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnFirstname.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        columnLastname.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        columnAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
        columnSpeciality.setCellValueFactory(new PropertyValueFactory<>("speciality"));
        columnImage.setCellValueFactory(new PropertyValueFactory<>("image"));

        btnAdd.setGraphic(new PetHelper().makeImageView("/images/add_green.png"));
        btnUpdate.setGraphic(new PetHelper().makeImageView("/images/save_green.png"));
        btnDelete.setGraphic(new PetHelper().makeImageView("/images/delete_red.png"));
        btnClose.setGraphic(new PetHelper().makeImageView("/images/close.png"));
        btnConsults.setGraphic(new PetHelper().makeImageView("/images/event.png"));
        btnPets.setGraphic(new PetHelper().makeImageView("/images/pet.png"));
        btnVets.setGraphic(new PetHelper().makeImageView("/images/vet.png"));
        btnBack.setGraphic(new PetHelper().makeImageView("/images/back.png"));

        setImageView();
        new PetHelper().addTip(imageViewVet, "Click to edit/upload image");

        Disabable.disableNodes(Arrays.asList(btnUpdate, btnDelete), true);
        fillTable();
        readTableRowData();
        fillComboBoxSearchBy();
        filterTable();
        addVet();
        updateVet();
        deleteVet();
        showPetWindow();
        showConsultsWindow();
        closeWindow();
        exitApp();
    }

    // reading data from database to table
    public void fillTable() {
        // getting list of vets from database
        List<Vet> vets = new VetDao().readVets();
        // converting list to observable
        ObservableList<Vet> oList = OList.makeObservableList(vets);
        // filling table with observable list
        OList.fillNode(tableViewVets, oList);
    }

    // reading table row data
    private void readTableRowData() {
        // adding event listener for selected rows in table
        tableViewVets.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (tableViewVets.getSelectionModel().getSelectedItem() != null) {
                // getting selected row index
                selectedRowIndex = tableViewVets.getSelectionModel().getSelectedIndex();
                // checking if record is the last
                isSelectedRowIsLast = Selectable.isLastRecordOfNode(tableViewVets);
                // reading table row data
                String firstName = tableViewVets.getSelectionModel().getSelectedItem().getFirstName();
                txtFieldFirstname.setText(firstName);
                String lastName = tableViewVets.getSelectionModel().getSelectedItem().getLastName();
                txtFieldLastname.setText(lastName);
                String address = tableViewVets.getSelectionModel().getSelectedItem().getAddress();
                txtFieldAddress.setText(address);
                String speciality = tableViewVets.getSelectionModel().getSelectedItem().getSpeciality();
                txtFieldSpeciality.setText(speciality);
                Image image = new PetHelper().byteArrayToImage(tableViewVets.getSelectionModel().getSelectedItem().getImage());
                imageViewVet.setImage(image);
                // enabling nodes
                Disabable.disableNodes(Arrays.asList(
                        btnUpdate,
                        btnDelete,
                        txtFieldFirstname,
                        txtFieldLastname,
                        txtFieldAddress,
                        txtFieldSpeciality,
                        imageViewVet), false);
            }
        });
    }

    // filling combo box with searching criteria
    public void fillComboBoxSearchBy() {
        OList.fillNode(comboBoxSearchBy, OList.makeObservableList(Arrays.asList("firstname", "lastname", "address", "speciality", "id")));
        Focusable.focusToFirstRecordOfNode(comboBoxSearchBy);
    }

    // filtering table
    public void filterTable() {
        FilteredList<Vet> flVet = new FilteredList<>(OList.makeObservableList(new VetDao().readVets()), vet -> true);
        OList.fillNode(tableViewVets, flVet);

        txtFieldSearch.setOnKeyReleased(event -> {
            switch (comboBoxSearchBy.getValue()) {
                case "firstname":
                    flVet.setPredicate(vet -> vet.getFirstName().toLowerCase().contains(txtFieldSearch.getText().toLowerCase().trim()));
                    break;
                case "lastname":
                    flVet.setPredicate(vet -> vet.getLastName().toLowerCase().contains(txtFieldSearch.getText().toLowerCase().trim()));
                    break;
                case "address":
                    flVet.setPredicate(vet -> vet.getAddress().toLowerCase().contains(txtFieldSearch.getText().toLowerCase().trim()));
                    break;
                case "speciality":
                    flVet.setPredicate(vet -> vet.getSpeciality().toLowerCase().contains(txtFieldSearch.getText().toLowerCase().trim()));
                    break;
                case "id":
                    flVet.setPredicate(vet -> vet.getId().toString().contains(txtFieldSearch.getText().toLowerCase().trim()));
                    break;
            }

            // removing focus from selected record of table
            tableViewVets.getSelectionModel().clearSelection();
            // clearing fields
            Clearable.clearNodes(Arrays.asList(txtFieldFirstname, txtFieldLastname, txtFieldAddress, txtFieldSpeciality, imageViewVet), "/images/addimage.png");
            // disabling buttons
            Disabable.disableNodes(Arrays.asList(btnUpdate, btnDelete), true);
        });
    }

    // adding new record in database
    public void addVet() {
        btnAdd.setOnAction(event -> {
            if (Empty.isNotEmptyAllNodes(Arrays.asList(txtFieldFirstname, txtFieldLastname, txtFieldAddress, txtFieldSpeciality))) {
                new VetDao().addVet(createVetPOJO());
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record added successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table filtering
                filterTable();
                // focusing to created race
                Focusable.focusToLastRecordOfNode(tableViewVets);
                // remove listeners
                Arrays.asList(
                        txtFieldFirstname,
                        txtFieldLastname,
                        txtFieldAddress,
                        txtFieldSpeciality).forEach(textField -> textField.textProperty().removeListener(tfListener));
            } else {
                if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.ERROR, "Input is required",
                        "Please fill out this field to continue", false))) {
                    Empty.focusToEmptyNodes(Arrays.asList(txtFieldFirstname, txtFieldLastname, txtFieldAddress, txtFieldSpeciality));
                    // adding listeners
                    Arrays.asList(
                            txtFieldFirstname,
                            txtFieldLastname,
                            txtFieldAddress,
                            txtFieldSpeciality).forEach(textField -> textField.textProperty().addListener(tfListener));
                }
            }
        });
    }

    // creating new POJO
    private Vet createVetPOJO() {
        return new Vet(
                txtFieldFirstname.getText(),
                txtFieldLastname.getText(),
                txtFieldAddress.getText(),
                txtFieldSpeciality.getText(),
                new PetHelper().imageToByteArray(imageViewVet.getImage()));
    }

    // updating record
    private void updateVet() {
        btnUpdate.setOnAction(event -> {
            if (Empty.isNotEmptyAllNodes(Arrays.asList(txtFieldFirstname, txtFieldLastname, txtFieldAddress, txtFieldSpeciality))) {
                new VetDao().updateVet(tableViewVets, txtFieldFirstname, txtFieldLastname, txtFieldAddress, txtFieldSpeciality, imageViewVet);
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record updated successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table
                filterTable();
                // focusing to updated row
                Focusable.focusToSelectedRow(tableViewVets, selectedRowIndex);
                // remove listeners
                Arrays.asList(
                        txtFieldFirstname,
                        txtFieldLastname,
                        txtFieldAddress,
                        txtFieldSpeciality).forEach(textField -> textField.textProperty().removeListener(tfListener));
            } else {
                if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.ERROR, "Input is required",
                        "Please fill out this field to continue", false))) {
                    Empty.focusToEmptyNodes(Arrays.asList(txtFieldFirstname, txtFieldLastname, txtFieldAddress, txtFieldSpeciality));
                    // adding listeners
                    Arrays.asList(
                            txtFieldFirstname,
                            txtFieldLastname,
                            txtFieldAddress,
                            txtFieldSpeciality).forEach(textField -> textField.textProperty().addListener(tfListener));
                }
            }
        });
    }

    // creating file chooser for image uploading
    public File selectFileToUpload() {
        FileChooser fileChooser = new FileChooser();
        // adding file filters
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG file (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPEG file (*.jpeg)", "*.jpeg"),
                new FileChooser.ExtensionFilter("JPG file (*.jpg)", "*.jpg"));
        return fileChooser.showOpenDialog(imageViewVet.getScene().getWindow());
    }

    // setting image to image view
    public void setImageView() {
        imageViewVet.setOnMouseClicked(event -> {
            try {
                File selectedFile = selectFileToUpload();
                if (selectedFile == null) {
                    System.out.println(" ");
                } else {
                    Image image = new Image(new FileInputStream(selectedFile));
                    imageViewVet.setImage(image);
                }
            } catch (FileNotFoundException e) {
                LoggerUtil.createLog(Level.SEVERE, "File not found", e);
            }
        });
    }

    // deleting record
    private void deleteVet() {
        btnDelete.setOnAction(event -> {
            if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.CONFIRMATION, "Confirmation",
                    "Are you sure you want to delete record from database?", false))) {
                new VetDao().deleteVet(tableViewVets);
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record deleted successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table
                filterTable();
                // focusing to record
                if (isSelectedRowIsLast) {
                    Focusable.focusToPrevRecordOfNode(tableViewVets, selectedRowIndex);
                } else {
                    Focusable.focusToSelectedRow(tableViewVets, selectedRowIndex);
                }
                // clearing search field
                Clearable.clearNodes(Collections.singletonList(txtFieldSearch), null);
                // clearing nodes on empty table
                Clearable.clearNodesOnEmptyNode(Empty.isEmptyTable(Collections.singletonList(tableViewVets)),
                        Arrays.asList(
                                txtFieldFirstname,
                                txtFieldLastname,
                                txtFieldAddress,
                                txtFieldSpeciality,
                                txtFieldSearch,
                                imageViewVet), "/images/addimage.png", isEmpty -> isEmpty);
                // disabling nodes on empty table
                Disabable.disableNodesOnEmptyNode(Empty.isEmptyTable(Collections.singletonList(tableViewVets)),
                        Arrays.asList(btnUpdate, btnDelete), isEmpty -> isEmpty);
            }
        });
    }

    // showing new window
    public void showStageAndWait(String pathToFxml) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(pathToFxml));
        try {
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to show stage", e);
        }
    }

    // managing pets
    private void showPetWindow() {
        btnPets.setOnAction(event -> showStageAndWait("/fxml/pet.fxml"));
    }

    // managing consults
    private void showConsultsWindow() {
        btnConsults.setOnAction(event -> showStageAndWait("/fxml/consult.fxml"));
    }

    // close window
    public void closeWindow() {
        Closeable.closeWindowOnButtonClick(btnBack);
    }

    // exit app
    public void exitApp() {
        Closeable.exitApp(btnClose);
    }
}
