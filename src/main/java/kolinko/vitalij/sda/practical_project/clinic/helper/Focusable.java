package kolinko.vitalij.sda.practical_project.clinic.helper;

import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;

public interface Focusable {
    static <T> void focusToFirstRecordOfNode(Node node) {
        ((ComboBox<T>) node).getSelectionModel().selectFirst();
    }

    static <T> void focusToLastRecordOfNode(Node node) {
        if (node instanceof TableView) {
            int lastRecordIndex = ((TableView<T>) node).getItems().size()-1;
            node.requestFocus();
            ((TableView<T>) node).getSelectionModel().selectLast();
            ((TableView<T>) node).getFocusModel().focus(lastRecordIndex);
            ((TableView<T>) node).scrollTo(lastRecordIndex);
        }
    }

    static <T> void focusToSelectedRow(Node node, int selectedRowIndex) {
        if (node instanceof TableView) {
            node.requestFocus();
            ((TableView<T>) node).getSelectionModel().select(selectedRowIndex);
            ((TableView<T>) node).getFocusModel().focus(selectedRowIndex);
            ((TableView<T>) node).scrollTo(selectedRowIndex);
        }
    }

    static void focusToComboBoxRandomRecord(Node node, Object record) {
        if (node instanceof ComboBox) {
            ((ComboBox) node).getSelectionModel().select(record);
        }
    }

    static <T> void focusToPrevRecordOfNode(Node node, int selectedRowIndex) {
        if (node instanceof TableView) {
            node.requestFocus();
            ((TableView<T>) node).getSelectionModel().select(selectedRowIndex-1);
            ((TableView<T>) node).getFocusModel().focus(selectedRowIndex-1);
            ((TableView<T>) node).scrollTo(selectedRowIndex-1);
        }
    }
}
