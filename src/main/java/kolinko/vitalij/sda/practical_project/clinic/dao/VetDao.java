package kolinko.vitalij.sda.practical_project.clinic.dao;

import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import kolinko.vitalij.sda.practical_project.clinic.entity.Vet;
import kolinko.vitalij.sda.practical_project.clinic.helper.PetHelper;
import kolinko.vitalij.sda.practical_project.clinic.util.HibernateUtil;
import kolinko.vitalij.sda.practical_project.clinic.util.LoggerUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class VetDao {
    // reading records from database
    public List<Vet> readVets() {
        List<Vet> vets = new ArrayList<>();
        Transaction transaction = null;
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            vets = session.createQuery("FROM Vet", Vet.class).list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to get records from database", e);
        }
        return vets;
    }

    // creating new record in database
    public void addVet(Vet vet) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            // creating new record in database
            session.save(vet);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to add new record to database", e);
        }
    }

    // updating record in database
    public void updateVet(TableView<Vet> tv, TextField tf1, TextField tf2, TextField tf3, TextField tf4, ImageView imageView) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Vet vetToUpdate = session.load(Vet.class, tv.getSelectionModel().getSelectedItem().getId());
            // setting row properties
            vetToUpdate.setFirstName(tf1.getText());
            vetToUpdate.setLastName(tf2.getText());
            vetToUpdate.setAddress(tf3.getText());
            vetToUpdate.setSpeciality(tf4.getText());
            // converting image to byte array
            byte[] imageData = new PetHelper().imageToByteArray(imageView.getImage());
            vetToUpdate.setImage(imageData);
            // updating record in database
            session.update(vetToUpdate);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to update record in database", e);
        }
    }

    // deleting record from database
    public void deleteVet(TableView<Vet> tableView) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Vet vetToDelete = session.load(Vet.class, tableView.getSelectionModel().getSelectedItem().getId());
            // deleting record from database
            session.delete(vetToDelete);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to delete record from database", e);
        }
    }
}
