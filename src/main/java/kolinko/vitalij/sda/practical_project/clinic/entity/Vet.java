package kolinko.vitalij.sda.practical_project.clinic.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "vets")
@Getter
@Setter
@NoArgsConstructor
public class Vet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String address;
    private String speciality;
    @Lob
    private byte[] image;

    @OneToMany(mappedBy = "vet", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Consult> consults;

    public Vet(String firstName, String lastName, String address, String speciality, byte[] image) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.speciality = speciality;
        this.image = image;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + ", " + speciality;
    }
}
