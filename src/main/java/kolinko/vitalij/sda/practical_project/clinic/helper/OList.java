package kolinko.vitalij.sda.practical_project.clinic.helper;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;

import java.util.List;

public interface OList {
    static <T> ObservableList<T> makeObservableList(List<T> list) {
        ObservableList<T> oList = FXCollections.observableArrayList();
        oList.setAll(list);
        return oList;
    }

    static <T> void fillNode(Node node, ObservableList<T> observableList) {
        if (node instanceof TableView) {
            ((TableView<T>) node).setItems(observableList);
        }
        if (node instanceof ComboBox) {
            ((ComboBox<T>) node).setItems(observableList);
        }
    }
}
