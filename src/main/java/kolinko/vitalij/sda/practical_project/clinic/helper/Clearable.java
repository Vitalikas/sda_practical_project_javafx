package kolinko.vitalij.sda.practical_project.clinic.helper;

import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.List;
import java.util.function.Predicate;

public interface Clearable {
    static void clearNodesOnEmptyNode(boolean isEmpty, List<Node> nodes, String pathToResources, Predicate<Boolean> predicate) {
        if (predicate.test(isEmpty)) {
            nodes.forEach(node -> {
                if (node instanceof ComboBox) {
                    ((ComboBox) node).getEditor().clear();
                }
                if (node instanceof DatePicker) {
                    ((DatePicker) node).setValue(null);
                }
                if (node instanceof CheckBox) {
                    ((CheckBox) node).setSelected(false);
                }
                if (node instanceof TextField) {
                    ((TextField) node).clear();
                }
                if (node instanceof ImageView) {
                    ((ImageView) node).setImage(new Image(Clearable.class.getResourceAsStream(pathToResources)));
                }
            });
        }
    }

    static void clearNodes(List<Node> nodes, String pathToResources) {
        nodes.forEach(node -> {
            if (node instanceof ComboBox) {
                ((ComboBox) node).valueProperty().setValue(null);
            }
            if (node instanceof DatePicker) {
                ((DatePicker) node).setValue(null);
            }
            if (node instanceof CheckBox) {
                ((CheckBox) node).setSelected(false);
            }
            if (node instanceof TextField) {
                ((TextField) node).clear();
            }
            if (node instanceof ImageView) {
                ((ImageView) node).setImage(new Image(Clearable.class.getResourceAsStream(pathToResources)));
            }
        });
    }
}
