package kolinko.vitalij.sda.practical_project.clinic.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "pets")
@Getter
@Setter
@NoArgsConstructor
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String race;
    private LocalDate birthdate;
    private Boolean isVaccinated;
    private String ownerName;
    @Lob
    private byte[] image;

    @OneToMany(mappedBy = "pet", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Consult> consults;

    public void addConsult(Consult consult) {
        consults.add(consult);
        consult.setPet(this);
    }

    public Pet(String race, LocalDate birthdate, Boolean isVaccinated, String ownerName, byte[] image) {
        this.race = race;
        this.birthdate = birthdate;
        this.isVaccinated = isVaccinated;
        this.ownerName = ownerName;
        this.image = image;
    }

    @Override
    public String toString() {
        return race + ", " + birthdate + ", ID: " + id;
    }
}
