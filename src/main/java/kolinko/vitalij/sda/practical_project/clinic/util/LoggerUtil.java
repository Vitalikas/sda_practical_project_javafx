package kolinko.vitalij.sda.practical_project.clinic.util;

import java.io.IOException;
import java.util.logging.*;

public class LoggerUtil {
    public static void createLog(Level level, String message, Exception e) {
        Logger logger = Logger.getLogger("log");
        FileHandler fh = null;
        try {
            fh = new FileHandler("src/main/logs/errors.log", true);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.addHandler(fh);
        } catch (IOException exception) {
            System.err.println("Log file not found");
            exception.printStackTrace();
        } finally {
            if (fh != null) {
                fh.close();
            }
        }
        logger.log(level, message, e);
    }
}
