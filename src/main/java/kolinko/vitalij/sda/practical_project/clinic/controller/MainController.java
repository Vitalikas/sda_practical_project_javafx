package kolinko.vitalij.sda.practical_project.clinic.controller;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import kolinko.vitalij.sda.practical_project.clinic.dao.ConsultDao;
import kolinko.vitalij.sda.practical_project.clinic.dao.PetDao;
import kolinko.vitalij.sda.practical_project.clinic.dao.VetDao;
import kolinko.vitalij.sda.practical_project.clinic.entity.Consult;
import kolinko.vitalij.sda.practical_project.clinic.entity.Pet;
import kolinko.vitalij.sda.practical_project.clinic.entity.Vet;
import kolinko.vitalij.sda.practical_project.clinic.helper.*;
import kolinko.vitalij.sda.practical_project.clinic.util.LoggerUtil;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class MainController implements Initializable {
    @FXML private TextField txtFieldSearch;
    @FXML private DatePicker datePickerFrom;
    @FXML private DatePicker datePickerTo;
    @FXML private Button btnClearDates;
    @FXML private Button btnClearText;
    @FXML public TableView<Consult> tableViewConsults;
    @FXML private TableColumn<Consult, Long> columnId;
    @FXML private TableColumn<Consult, LocalDate> columnDate;
    @FXML private TableColumn<Consult, String> columnDescription;
    @FXML private TableColumn<Consult, Vet> columnVet;
    @FXML private TableColumn<Consult, Pet> columnPet;
    @FXML private ImageView imageViewPet;
    @FXML public DatePicker datePickerConsultDate;
    @FXML public TextField txtFieldDescription;
    @FXML private ComboBox<Vet> comboBoxVet;
    @FXML private ComboBox<Pet> comboBoxPet;
    @FXML public Button btnAdd;
    @FXML public Button btnUpdate;
    @FXML public Button btnDelete;
    @FXML public Button btnConsults;
    @FXML public Button btnPets;
    @FXML public Button btnVets;
    @FXML public Button btnClose;

    int selectedRowIndex;
    boolean isSelectedRowIsLast;

    // creating listener for date picker
    ChangeListener<LocalDate> dpListener = (observable, oldValue, newValue) -> {
        if (newValue != null) {
            datePickerConsultDate.setStyle("-fx-background-color: #ffffff");
        }
        if (datePickerConsultDate.getValue() == null) {
            datePickerConsultDate.setStyle("-fx-border-color: red; -fx-focus-color: red;");
        }
    };
    // creating listener for text field
    ChangeListener<String> tfListener = (observable, oldValue, newValue) -> {
        if (newValue != null) {
            txtFieldDescription.setStyle("-fx-background-color: #ffffff");
        }
        if (txtFieldDescription.getText().isEmpty()) {
            txtFieldDescription.setStyle("-fx-text-box-border: red; -fx-focus-color: red;");
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        columnVet.setCellValueFactory(new PropertyValueFactory<>("vet"));
        columnPet.setCellValueFactory(new PropertyValueFactory<>("pet"));

        btnAdd.setGraphic(new PetHelper().makeImageView("/images/add_green.png"));
        btnUpdate.setGraphic(new PetHelper().makeImageView("/images/save_green.png"));
        btnDelete.setGraphic(new PetHelper().makeImageView("/images/delete_red.png"));
        btnPets.setGraphic(new PetHelper().makeImageView("/images/pet.png"));
        btnVets.setGraphic(new PetHelper().makeImageView("/images/vet.png"));
        btnClose.setGraphic(new PetHelper().makeImageView("/images/close.png"));

        Disabable.disableNodes(Arrays.asList(btnUpdate, btnDelete), true);

        fillTable();
        readTableRowData();
        filterTable();
        clearDates();
        clearSearchCriteria();
        fillComboBoxVet();
        fillComboBoxPet();
        addConsult();
        updateConsult();
        deleteConsult();
        showPetWindow();
        showVetWindow();
        exitApp();
    }

    // reading data from database to table
    public void fillTable() {
        // getting list of consults from database
        List<Consult> consults = new ConsultDao().readConsults();
        // converting list to observable
        ObservableList<Consult> oList = OList.makeObservableList(consults);
        // filling table with observable list
        OList.fillNode(tableViewConsults, oList);
    }

    // reading table row data
    private void readTableRowData() {
        // adding event listener for selected rows in table
        tableViewConsults.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (tableViewConsults.getSelectionModel().getSelectedItem() != null) {
                // getting selected row index
                selectedRowIndex = tableViewConsults.getSelectionModel().getSelectedIndex();
                // checking if record is the last
                isSelectedRowIsLast = Selectable.isLastRecordOfNode(tableViewConsults);
                // focusing to selected race
                Focusable.focusToComboBoxRandomRecord(comboBoxVet, tableViewConsults.getSelectionModel().getSelectedItem().getVet());
                Focusable.focusToComboBoxRandomRecord(comboBoxPet, tableViewConsults.getSelectionModel().getSelectedItem().getPet());
                // reading table row data
                LocalDate doc = tableViewConsults.getSelectionModel().getSelectedItem().getDate();
                datePickerConsultDate.setValue(doc);
                String description = tableViewConsults.getSelectionModel().getSelectedItem().getDescription();
                txtFieldDescription.setText(description);
                Vet vet = tableViewConsults.getSelectionModel().getSelectedItem().getVet();
                comboBoxVet.valueProperty().setValue(vet);
                Pet pet = tableViewConsults.getSelectionModel().getSelectedItem().getPet();
                comboBoxPet.valueProperty().setValue(pet);
                Image image = new PetHelper().byteArrayToImage(tableViewConsults.getSelectionModel().getSelectedItem().getPet().getImage());
                imageViewPet.setImage(image);
                // enabling nodes
                Disabable.disableNodes(Arrays.asList(
                        btnUpdate,
                        btnDelete,
                        datePickerConsultDate,
                        txtFieldDescription,
                        comboBoxVet,
                        comboBoxPet), false);
            }
        });
    }

    // filtering by date
    public void filterTable() {
        FilteredList<Consult> flConsult = new FilteredList<>(OList.makeObservableList(new ConsultDao().readConsults()));
        OList.fillNode(tableViewConsults, flConsult);

        // bind predicate based on date picker choices
        flConsult.predicateProperty().bind(Bindings.createObjectBinding(() -> {
            LocalDate dateFrom = datePickerFrom.getValue();
            LocalDate dateTo = datePickerTo.getValue();
            String searchBy = txtFieldSearch.getText().toLowerCase();

            // get final values != null
            final LocalDate finalMin = dateFrom == null ? LocalDate.MIN : dateFrom;
            final LocalDate finalMax = dateTo == null ? LocalDate.MAX : dateTo;

            // removing focus from selected record of table
            tableViewConsults.getSelectionModel().clearSelection();
            // clearing fields
            Clearable.clearNodes(Arrays.asList(datePickerConsultDate, txtFieldDescription, comboBoxVet, comboBoxPet, imageViewPet), "/images/addimage.png");
            // disabling buttons
            Disabable.disableNodes(Arrays.asList(btnUpdate, btnDelete), true);

            // values for dates need to be in the interval [finalMin, finalMax]
            return consult ->
                    !finalMin.isAfter(consult.getDate()) && !finalMax.isBefore(consult.getDate()) &&
                    (consult.getId().toString().contains(searchBy.toLowerCase().trim()) ||
                    consult.getDescription().toLowerCase().contains(searchBy.toLowerCase().trim()) ||
                    consult.getVet().toString().toLowerCase().contains(searchBy.toLowerCase().trim()) ||
                    consult.getPet().toString().toLowerCase().contains(searchBy.toLowerCase().trim()));
        }, datePickerFrom.valueProperty(), datePickerTo.valueProperty(), txtFieldSearch.textProperty()));
    }

    // clearing filter dates
    public void clearDates() {
        btnClearDates.setOnAction(event -> {
            if (datePickerFrom.getValue() != null || datePickerTo.getValue() != null) {
                Clearable.clearNodes(Arrays.asList(datePickerFrom, datePickerTo), null);
            }
        });
    }

    // clearing searching criteria
    public void clearSearchCriteria() {
        btnClearText.setOnAction(event -> {
            if (!txtFieldSearch.getText().isEmpty()) {
                Clearable.clearNodes(Collections.singletonList(txtFieldSearch), null);
            }
        });
    }

    // filling combo box Vet
    public void fillComboBoxVet() {
        OList.fillNode(comboBoxVet, OList.makeObservableList(new VetDao().readVets()));
    }

    // filling combo box Pet
    public void fillComboBoxPet() {
        OList.fillNode(comboBoxPet, OList.makeObservableList(new PetDao().readPets()));
    }

    // adding new record in database
    public void addConsult() {
        btnAdd.setOnAction(event -> {
            if (Empty.isNotEmptyAllNodes(Arrays.asList(datePickerConsultDate, txtFieldDescription))) {
                System.out.println(comboBoxVet.getValue());
                new ConsultDao().addConsult(createConsult());
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record added successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table filtering
                filterTable();
                // focusing to created race
                Focusable.focusToLastRecordOfNode(tableViewConsults);
                // remove listeners
                datePickerConsultDate.valueProperty().removeListener(dpListener);
                txtFieldDescription.textProperty().removeListener(tfListener);
            } else {
                if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.ERROR, "Input is required",
                        "Please fill out this field to continue", false))) {
                    Empty.focusToEmptyNodes(Arrays.asList(datePickerConsultDate, txtFieldDescription));
                    // adding listeners
                    datePickerConsultDate.valueProperty().addListener(dpListener);
                    txtFieldDescription.textProperty().addListener(tfListener);
                }
            }
        });
    }

    // creating new object
    private Consult createConsult() {
        Consult consult = new Consult(datePickerConsultDate.getValue(), txtFieldDescription.getText());
        consult.setVet(comboBoxVet.getValue());
        consult.setPet(comboBoxPet.getValue());
        return consult;
    }

    // updating record
    private void updateConsult() {
        btnUpdate.setOnAction(event -> {
            if (Empty.isNotEmptyAllNodes(Arrays.asList(datePickerConsultDate, txtFieldDescription))) {
                new ConsultDao().updateConsult(tableViewConsults, datePickerConsultDate, txtFieldDescription, comboBoxVet, comboBoxPet);
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record updated successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table
                filterTable();
                // focusing to updated row
                Focusable.focusToSelectedRow(tableViewConsults, selectedRowIndex);
                // remove listeners
                datePickerConsultDate.valueProperty().removeListener(dpListener);
                txtFieldDescription.textProperty().removeListener(tfListener);
            } else {
                if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.ERROR, "Input is required",
                        "Please fill out this field to continue", false))) {
                    Empty.focusToEmptyNodes(Arrays.asList(datePickerConsultDate, txtFieldDescription));
                    // adding listeners
                    datePickerConsultDate.valueProperty().addListener(dpListener);
                    txtFieldDescription.textProperty().addListener(tfListener);
                }
            }
        });
    }

    // deleting record
    private void deleteConsult() {
        btnDelete.setOnAction(event -> {
            if (Alerts.isConfirmed(Alerts.showAlert(Alert.AlertType.CONFIRMATION, "Confirmation",
                    "Are you sure you want to delete record from database?", false))) {
                new ConsultDao().deleteConsult(tableViewConsults);
                // getting stage
                Stage msgWindow = new PetHelper().getSuccessMsg("Record deleted successfully");
                // showing success message
                msgWindow.show();
                // auto closing message window on custom timer
                Closeable.closeWindowsAfterDelay(Collections.singletonList(msgWindow), 2.0);
                // refreshing table
                fillTable();
                // adding listener for table
                filterTable();
                // focusing to record
                if (isSelectedRowIsLast) {
                    Focusable.focusToPrevRecordOfNode(tableViewConsults, selectedRowIndex);
                } else {
                    Focusable.focusToSelectedRow(tableViewConsults, selectedRowIndex);
                }
                // clearing search field
                Clearable.clearNodes(Collections.singletonList(txtFieldSearch), null);
                // clearing nodes on empty table
                Clearable.clearNodesOnEmptyNode(Empty.isEmptyTable(Collections.singletonList(tableViewConsults)),
                        Arrays.asList(
                                datePickerConsultDate,
                                txtFieldDescription,
                                comboBoxVet,
                                comboBoxPet,
                                txtFieldSearch,
                                imageViewPet), "/images/addimage.png", isEmpty -> isEmpty);
                // disabling nodes on empty table
                Disabable.disableNodesOnEmptyNode(Empty.isEmptyTable(Collections.singletonList(tableViewConsults)),
                        Arrays.asList(btnUpdate, btnDelete), isEmpty -> isEmpty);
            }
        });
    }

    // showing new window
    public void showStageAndWait(String pathToFxml) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(pathToFxml));
        try {
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            // logging errors
            LoggerUtil.createLog(Level.SEVERE, "Failed to show stage", e);
        }
    }

    // managing pets
    private void showPetWindow() {
        btnPets.setOnAction(event -> {
            showStageAndWait("/fxml/pet.fxml");
            // refreshing table data
            fillTable();
            // refreshing combo box pet
            fillComboBoxPet();
            // adding listener for table filtering
            filterTable();
            // focusing to updated row
            Focusable.focusToSelectedRow(tableViewConsults, selectedRowIndex);
        });
    }

    // managing vets
    private void showVetWindow() {
        btnVets.setOnAction(event -> {
            showStageAndWait("/fxml/vet.fxml");
            // refreshing table data
            fillTable();
            // refreshing combo box pet
            fillComboBoxPet();
            // adding listener for table filtering
            filterTable();
            // focusing to updated row
            Focusable.focusToSelectedRow(tableViewConsults, selectedRowIndex);
        });
    }

    // exit app
    public void exitApp() {
        Closeable.exitApp(btnClose);
    }
}
