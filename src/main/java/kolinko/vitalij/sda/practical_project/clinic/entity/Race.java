package kolinko.vitalij.sda.practical_project.clinic.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "races")
@Getter
@Setter
@NoArgsConstructor
public class Race {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String race;

    public Race(String race) {
        this.race = race;
    }

    @Override
    public String toString() {
        return race;
    }
}
